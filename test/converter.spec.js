// Unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("Converts to the basic colors", () => {
            const rRGB = converter.hexToRGB("#100000").r;
            const gRGB = converter.hexToRGB("#000100").g;
            const bRGB = converter.hexToRGB("#0000ff").b;

            expect(rRGB).to.equal(16);
            expect(gRGB).to.equal(1);
            expect(bRGB).to.equal(255);
        });
    });
});